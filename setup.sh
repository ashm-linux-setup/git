#!/usr/bin/env bash

if [ -z "$ASHM_UTILS_BASHRC" ]; then

    echo "Error [git steup]: ashm bashrc utils have not been setup"
    exit 1
fi

if [ -z "$XDG_BIN_HOME" ] || ! [ -d "$XDG_BIN_HOME" ]; then

    echo "Error [git setup]: XDG_BIN_HOME has not been setup" >&2
    exit 1
fi

if [ -z "$ASHM_LOCAL_MANPAGE_HOME" ] || ! [ -d "$ASHM_LOCAL_MANPAGE_HOME" ]; then

    echo "Error [git setup]: local man pages directory has not been set up" >&2
    exit 1
fi


function install_man() {

    man_file_name=$(basename "$1")
    man_file_directory=$(realpath $(dirname "${BASH_SOURCE}"))

    gzip -c "${man_file_directory}/${man_file_name}" > "${man_file_directory}/${man_file_name}.gz"
    mv -f "${man_file_directory}/${man_file_name}.gz" "${ASHM_LOCAL_MANPAGE_HOME}/$2"
}


script_dir=$(realpath $(dirname "${BASH_SOURCE}"))
link_target_file_name="alias_git.sh"

rm -f "${XDG_BIN_HOME}/${link_target_file_name}"
ln -s "${script_dir}/${link_target_file_name}" "${XDG_BIN_HOME}/${link_target_file_name}"

add_section_to_bashrc "git"
source_file_in_bashrc_section "$link_target_file_name" "git"
install_man "./ashm_git.7" "man7"

