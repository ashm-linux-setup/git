#!/usr/bin/env bash

alias gt_alias="alias | sed 's/^alias //' | grep gt"
alias g="git"
alias gtst="git status"
alias gtsh="git show"
alias gtshn="git show --name-only"
alias gtlg="git log"
alias gtlg1="git log --oneline"
alias gtbr="git branch"
alias gtsw="git switch"
alias gtdf="git diff"
alias gtad="git add"
alias gtcm="git commit"
alias gtrstr="git restore"
alias gtrset="git reset"
alias gtft="git fetch"
alias gtpl="git pull"
alias gtph="git push"


if (command -v fzf > /dev/null) && (command -v bat > /dev/null); then

    function git_log_with_preview() {

        # show commit is fzf with preview and

        git log \
            --reverse \
    	--color=always \
    	--format="%C(cyan)%h %C(blue)%ar%C(auto)%d \
    	          %C(yellow)%s%+b %C(black)%ae" "$@" |
        fzf -i -e +s \
            --reverse \
    	--tiebreak=index \
    	--no-multi \
    	--ansi \
    	--preview="echo {} |
    	           grep -o '[a-f0-9]\{7\}' |
    		   head -1 |
    		   xargs -I % sh -c 'git show --color=always % |
    		                     bat'" |
        grep -o '^[a-f0-9]\{7\}'
    }
    export -f git_log_with_preview

    alias gtlpv="git_log_with_preview"
fi


if command -v bat > /dev/null; then

    git config --global clore.pager 'bat'
fi

if command -v delta > /dev/null; then

    git config --global diff.tool 'delta'
    git config --global merge.tool 'delta'
fi

